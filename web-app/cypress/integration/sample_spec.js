// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('The Traffic web site configuration', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click(); //click sur le bouton configuration
        cy.url().should('include', "#configuration") //vérifie que le nouvel URL contient bien #configuration
    })
})

describe('There are 28 segments', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click(); //click sur le bouton configuration
        cy.get('form[data-kind="segment"]').should('have.length', 28); //vérifie que le tableau segment contient bien 28 lignes
    })
})

describe('There are 0 vehicle', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click(); //click sur le bouton configuration
        cy.get('table').contains('No vehicle available'); //vérifie que le tableau de véhicle est vide
    })
})

describe('Change speed segment', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click(); //click sur le bouton configuration
        cy.get('input[id="segment_speed_5"]').clear().type("30"); //remplie la vitesse du segment a 5
        cy.get('button[id="segment_validate_5"]').click(); // valide les changement
        cy.get('.modal-footer > .btn').click(); //valide la validation
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements'); //récupére la routes elements
        cy.get('@elements').should((response) => {
            expect(response.body['segments']['4']).to.have.property('speed', 30); //vérifie dans le json/côté serveur que la speed a bien était changé
        });
    })
})


describe('roundabout', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click();
        cy.get('input[id="capacity_31"]').clear().type("4"); //change la capacité du rond point a 5
        cy.get('input[id="duration_31"]').clear().type("15"); //change ca durée a 15
        cy.get('button[id="validate_31"]').click();//valide les changements
        cy.get('.modal-footer > .btn').click(); //valide la validation
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements');//récupére la routes elements
        cy.get('@elements').should((response) => {
            //vérifie que les 2 données sont bien changé dans le json/côté serveur
            expect(response.body['crossroads']['2']).to.have.property('capacity', 4);
            expect(response.body['crossroads']['2']).to.have.property('duration', 15);
        });
    })
})


describe('trafficlight #29', () => {
    it('successfully loads', () => {
        cy.get('input[id="orange_29"]').clear().type('4'); //change la durée du orange a 4
        cy.get('input[id="green_29"]').clear().type('40'); // change la durée du vert a 40
        cy.get('input[id="next_29"]').clear().type('8'); // change la durée de next_passage a 8
        cy.get('button[id="validate_29"]').click(); //valide les changements
        cy.get('.modal-footer > .btn').click(); // valide la validation
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements');//récupére la routes elements
        cy.get('@elements').should((response) => {
            //vérifie que les 3 données sont bien changé dans le json/côté serveur
            expect(response.body['crossroads']['0']).to.have.property('greenDuration', 40);
            expect(response.body['crossroads']['0']).to.have.property('orangeDuration', 4);
            expect(response.body['crossroads']['0']).to.have.property('nextPassageDuration', 8);
        });
    })
})


describe('add vehicle', () => {
    it('successfully loads', () => {
        //ajoute un véhicle(origine: S5/Destination: S26/Départ: 50)
        cy.get('input[id="new_origin"]').clear().type('5');
        cy.get('input[id="new_destination"]').clear().type('26');
        cy.get('input[id="new_time"]').clear().type('50');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S19/Destination: S8/Départ: 200)
        cy.get('input[id="new_origin"]').clear().type('19');
        cy.get('input[id="new_destination"]').clear().type('8');
        cy.get('input[id="new_time"]').clear().type('200');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S27/Destination: S2/Départ: 150)
        cy.get('input[id="new_origin"]').clear().type('27');
        cy.get('input[id="new_destination"]').clear().type('2');
        cy.get('input[id="new_time"]').clear().type('150');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles');
        cy.get('@vehicles').should((response) => {
            //vérifie la speed des 3 véhicles ajouter côté serveur
            expect(response.body['200.0'][0]).to.have.property('speed', 0);
            expect(response.body['150.0'][0]).to.have.property('speed', 0);
            expect(response.body['50.0'][0]).to.have.property('speed', 0);
        });

        //vérifie la speed des 3 véhicles ajouter côté client
        cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0');
        cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0');
        cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0');
    })
})

describe('check simulation', () => {
    it('successfully loads', () => {
        cy.get('a[href="#simulation"]').click();
        //vérifie que tous les vehicles sont a l'arret (côté client)
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0');
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0');
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0');

        cy.get('input[id="simulation_time"]').clear().type(120); //régle la simulation a 120 secondes
        cy.get('button[id="run_simulation"]').click(); //lance la simulation
        cy.get('.progress-bar', {timeout: 50000}).should('have.attr', 'aria-valuenow', 100); //attend que la simulation se termine en vérifiant que la progress bar arrive a 100%
        //vérifie que seul le 1er véhicle est en mouvement
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block")
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("play_circle_filled")
    })
})

describe('init simulation', () => {
    it('successfully loads', () => {
        cy.get('a[href="#simulation"]').click();
        cy.reload(); //recharge la page pour réinitialiser la simulation
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles'); // récupére la route véhicle
        cy.get('@vehicles').should((response) => {
            expect(response.body).to.be.empty; //vérifie qu'aucun véhicle n'est enregistrer coté serveur
        });

        cy.get('a[href="#configuration"]').click();//accéde a la page de configuration
        //ajoute un véhicle(origine: S5/Destination: S26/Départ: 50)
        cy.get('input[id="new_origin"]').clear().type('5');
        cy.get('input[id="new_destination"]').clear().type('26');
        cy.get('input[id="new_time"]').clear().type('50');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S19/Destination: S8/Départ: 200)
        cy.get('input[id="new_origin"]').clear().type('19');
        cy.get('input[id="new_destination"]').clear().type('8');
        cy.get('input[id="new_time"]').clear().type('200');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S27/Destination: S2/Départ: 150)
        cy.get('input[id="new_origin"]').clear().type('27');
        cy.get('input[id="new_destination"]').clear().type('2');
        cy.get('input[id="new_time"]').clear().type('150');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        cy.get('a[href="#simulation"]').click();
        cy.get('input[id="simulation_time"]').clear().type(500); //régle la simulation a 500 secondes
        cy.get('button[id="run_simulation"]').click(); //lance la simulation
        cy.get('.progress-bar',  {timeout: 100000}).should('have.attr', 'aria-valuenow', 100); //attend la fin de simulation

        //vérifie qu'aucun véhicles ne bouge
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");
    })
})

describe('story 10', () => {
    it('successfully loads', () => {
        cy.reload(); //recharge la page pour réinitialiser la simulation
        cy.get('a[href="#simulation"]').click();        
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles');
        cy.get('@vehicles').should((response) => {
            expect(response.body).to.be.empty;
        });

        cy.get('a[href="#configuration"]').click();//accéde a la page de configuration
        //ajoute un véhicle(origine: S5/Destination: S26/Départ: 50)
        cy.get('input[id="new_origin"]').clear().type('5');
        cy.get('input[id="new_destination"]').clear().type('26');
        cy.get('input[id="new_time"]').clear().type('50');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S5/Destination: S26/Départ: 80)
        cy.get('input[id="new_origin"]').clear().type('5');
        cy.get('input[id="new_destination"]').clear().type('26');
        cy.get('input[id="new_time"]').clear().type('80');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        //ajoute un véhicle(origine: S5/Destination: S26/Départ: 80)
        cy.get('input[id="new_origin"]').clear().type('5');
        cy.get('input[id="new_destination"]').clear().type('26');
        cy.get('input[id="new_time"]').clear().type('80');
        cy.get('button[id="create_car"]').click();
        cy.get('.modal-footer > .btn').click();

        cy.get('a[href="#simulation"]').click();
        cy.get('input[id="simulation_time"]').clear().type(200); //régle la simulation a 200 secondes
        cy.get('button[id="run_simulation"]').click(); //lance la simulation
        cy.get('.progress-bar',  {timeout: 50000}).should('have.attr', 'aria-valuenow', 100); //attend la fin de simulation

        //vérifie la position de fin de chaque véhicles
        cy.get('tbody > :nth-child(1) > :nth-child(5)').contains("29")
        cy.get('tbody > :nth-child(2) > :nth-child(5)').contains("29")
        cy.get('tbody > :nth-child(3) > :nth-child(5)').contains("17")

    })
})